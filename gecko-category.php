<?php
/**
 * Plugin Name: Gecko Category
 * Plugin URI: https://bitbucket.org/geckodesigns/gecko-category-widget/wiki/Home
 * Description: Gecko modifications to category taxonomy. Adds additional fields and creates a new category widget
 * Version: 1.1.0
 * Author: Gecko Designs
 * Author URI: https://geckodesigns.com
 * Requires at least: 4.4
 * Tested up to: 4.7
 *
 * Text Domain: gecko-category
 * Domain Path: 
 *
 * @package GeckoCategory
 * @category Core
 * @author Dwayne Parton
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Define Constants Here
 */
define( 'GECKOCATEGORY__FILE', __FILE__);

/**
 * Register plugin with Gecko Command. Nothing will happen if Gecko Command is not active.
 * It's ok to use inline functions here because there's no need to unhook.
 * Just disable the plugin
 */
add_action('gecko/register/plugin', function(){
	new Gecko\Category\Init();
});

/**
 * If gecko comand is not installed notify the user
 * It's ok to use inline functions here because there's no need to unhook.
 * Just disable the plugin
 */
add_action('init', function(){
	// Make sure GeckoCommand is active
	if (!function_exists('gecko_command')){
		add_action( 'admin_notices', function() {
			$class = 'notice notice-error';
			$message = __( 'Gecko Age Verification requires Gecko Command. Please contact Gecko and we will help you fix this.', 'gecko-category-widget' );
			printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) ); 
		});
	}
});