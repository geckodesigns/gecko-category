<?php
/**
 * Initiate.
 *
 * @author   Dwayne Parton
 * @category Class
 * @package  GeckoCategory
 * @version  0.0.1
 */
namespace Gecko\Category; 

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


/**
 * Main Class.
 */
class Init{

	/**
	 * Constructor.
	 */
	public function __construct() {
		// Register and load the widget
		add_action( 'widgets_init', array($this, 'register_widget'));
		// Register Fields
		self::register_fields();
		// Run Updater
		add_action( 'init', array($this, 'updater'));
	}

	/**
	 * Updater
	 */
	public function updater() {
		// Get the plugin data so we don't have to remember to update the version
		if(is_admin() && current_user_can('administrator')){
			new \Gecko\Command\Updater(GECKOCATEGORY__FILE, 'geckodesigns', 'gecko-category');
		}
	}


	/**
	 * Register Widget
	 */
	public function register_widget() {
		register_widget(new Widget);
	}

	/**
	 * Add ACF Fields
	 * https://www.advancedcustomfields.com/resources/register-fields-via-php/
	 * The easiest way is to use the gui and then export group via acf in php form
	 */
	public static function register_fields() {
		if( function_exists('acf_add_local_field_group') ):
			acf_add_local_field_group(array(
				'key' => 'group_5b06e5854be3c',
				'title' => 'Gecko Category Meta',
				'fields' => array(
					array(
						'key' => 'field_5b06e59aa4e57',
						'label' => 'Featured Image',
						'name' => 'featured_image',
						'type' => 'image',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'return_format' => 'array',
						'preview_size' => 'thumbnail',
						'library' => 'all',
						'min_width' => '',
						'min_height' => '',
						'min_size' => '',
						'max_width' => '',
						'max_height' => '',
						'max_size' => '',
						'mime_types' => '',
					),
				),
				'location' => array(
					array(
						array(
							'param' => 'taxonomy',
							'operator' => '==',
							'value' => 'category',
						),
					),
				),
				'menu_order' => 0,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => 1,
				'description' => '',
			));
		endif;
	}

}