<?php
/**
 * Widget.
 *
 * @author   Dwayne Parton
 * @category Class
 * @package  GeckoCategory
 * @version  0.0.1
 */
namespace Gecko\Category; 

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


/**
 * Widget Class.
 */
class Widget extends \WP_Widget{

	/**
	 * Constructor.
	 */
	public function __construct() {
		$widget_id = 'gecko_category_widget';
		$widget_name = __('Gecko Categories', 'gecko-category-widget');
		$widget_options = array( 
			'classname' => 'gecko-category-widget',
			'description' => __( 'Displays Categories with hookable info', 'gecko-category-widget' ),
		);
		parent::__construct(
 			$widget_id, 
			$widget_name, 
			$widget_options
		);
	}

	/**
	 * Backend UI Form
	 */
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		} else {
			$title = __( 'Categories', 'gecko-category-widget' );
		}
		// Widget admin form
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<?php 
	}

	/**
	 * Save/Update Form
	 */
	public function update( $new_instance, $old_instance ) {       
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
	}

	/**
	 * Widget HTML
	 */
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
		if ( ! empty( $title ) )
		echo $args['before_title'] . $title . $args['after_title'];
		// Main Widget HTML
		$cats = get_terms( array(
			'taxonomy' => 'category',
			'hide_empty' => true,
		));
		echo '<div class="gecko-category-widget__cats">';
		foreach($cats as $cat):
			$image = get_field('featured_image', $cat);
			ob_start();
			?>
			<div class="gecko-category-widget__cat" style="background-image: url(<?php echo $image['sizes']['medium']; ?>);">
				<a href="<?php echo get_term_link($cat); ?>" class="gecko-category-widget__cat-name"><?php echo $cat->name; ?></a>
			</div>
			<?php
			$template = ob_get_contents();
			ob_end_clean();
			$template = apply_filters('gecko/category/widget/template', $template, $cat);
			echo $template;
		endforeach;
		echo '</div>';
		// After Widget Wrap
		echo $args['after_widget'];
	}

}