# Gecko Category

Adds additional functionality to the default post category

* Adds Featured Image meta with key: featured_image
* Adds widget for listing categories with customizable template.

## Modifying the Category Widget

You can modify the category loop template with the following

```
add_filter('gecko/category/widget/template', 'gecko_mod_cat_template', 10, 2);
function gecko_mod_cat_template($template, $cat){
	$image = get_field('featured_image', $cat);
	ob_start();
	?>
	<div class="gecko-category-widget__cat" style="background-image: url(<?php echo $image['url']; ?>);">
		<a href="<?php echo get_term_link($cat); ?>" class="gecko-category-widge__cat-name"><?php echo $cat->name; ?></a>
	</div>
	<?php
	$template = ob_get_contents();
	ob_end_clean();
	return $template;
}
```